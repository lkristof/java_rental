package domain;

import java.math.BigDecimal;
import domain.Car;

public class VehicleBuilder {
	private String brand;
	private String model;
	private String plateNumber;
	private BigDecimal dailyPrice;
	private boolean available;
	private LicenseCategory licenseCategory;
	
	public VehicleBuilder() {
		super();		
	}

	public VehicleBuilder setBrand(String brand) {
		this.brand = brand;
		return this;
	}

	public VehicleBuilder setModel(String model) {
		this.model = model;
		return this;
	}

	public VehicleBuilder setPlateNumber(String plateNumber) {
		this.plateNumber = plateNumber;
		return this;
	}

	public VehicleBuilder setDailyPrice(BigDecimal dailyPrice) {
		this.dailyPrice = dailyPrice;
		return this;
	}

	public VehicleBuilder setAvailable(boolean available) {
		this.available = available;
		return this;
	}

	public VehicleBuilder setLicenseCategory(LicenseCategory licenseCategory) {
		this.licenseCategory = licenseCategory;
		return this;
	}
	
	public Car getCar() {
		return new  Car(this.brand, this.model, this.plateNumber, this.dailyPrice, this.available,
				this.licenseCategory);
	}
	
	public Truck getTruck() {
		return new Truck(this.brand, this.model, this.plateNumber, this.dailyPrice, this.available,
				this.licenseCategory);
	}
	
	public MotorBike getMotorBike() {
		return new MotorBike(this.brand, this.model, this.plateNumber, this.dailyPrice, this.available,
				this.licenseCategory);
	}
	
	
}
