package domain;

import java.math.BigDecimal;

public class Vehicle {

	private String brand;
	private String model;
	private String plateNumber;
	private BigDecimal dailyPrice;
	private boolean available;
	private LicenseCategory licenseCategory;
	
	public Vehicle(String brand, String model, String plateNumber, BigDecimal dailyPrice, boolean available,
			LicenseCategory licenseCategory) {
		super();
		this.brand = brand;
		this.model = model;
		this.plateNumber = plateNumber;
		this.dailyPrice = dailyPrice;
		this.available = available;
		this.licenseCategory = licenseCategory;
	}
	
	
}
