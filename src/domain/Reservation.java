package domain;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class Reservation {

	private LocalDateTime startDate;
	private LocalDateTime endDate;
	private BigDecimal price;
	private Customer customer;
	private Vehicle vehicle;
	
	public Reservation(LocalDateTime startDate, LocalDateTime endDate, BigDecimal price, Customer customer, Vehicle vehicle) {
		super();
		this.startDate = startDate;
		this.endDate = endDate;
		this.price = price;
		this.customer = customer;
		this.vehicle = vehicle;
	}
	
	
}
