package domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Customer extends User {

	private String name;
	private BigDecimal balance;
	private List<Reservation> reservations;
	
	public Customer(String name, String email, String password) {
		super(email, password);
	}

}
