package service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import domain.Car;
import domain.Customer;
import domain.MotorBike;
import domain.Reservation;
import domain.Truck;
import domain.LicenseCategory;
import domain.Vehicle;
import domain.VehicleBuilder;

public class DataService {

	private List<Vehicle> vehicles;
	private List<Reservation> reservations;
	private List<Customer> customers;
	
	public DataService() {
		super();	
		this.vehicles = new ArrayList<Vehicle>();
		this.reservations = new ArrayList<Reservation>();
		this.customers = new ArrayList<Customer>();
	}
	
	public void buildCars() {
		VehicleBuilder vehicleBuilder = new VehicleBuilder();
		Car car = vehicleBuilder.setBrand("Skoda")
				.setModel("Octavia")
				.setAvailable(true)
				.setDailyPrice(new BigDecimal(5000))
				.setLicenseCategory(LicenseCategory.B)
				.setPlateNumber("SKODA123")
				.getCar();								
		this.vehicles.add(car);
		
		car = vehicleBuilder.setBrand("Opel")
				.setModel("Corsa")
				.setAvailable(true)
				.setDailyPrice(new BigDecimal(4000))
				.setLicenseCategory(LicenseCategory.B)
				.setPlateNumber("OPEL123")
				.getCar();
		this.vehicles.add(car);
		
		Truck truck = vehicleBuilder.setBrand("Scania")
				.setModel("Big")
				.setAvailable(true)
				.setDailyPrice(new BigDecimal(6700))
				.setLicenseCategory(LicenseCategory.C)
				.setPlateNumber("SCANIA123")
				.getTruck();
		this.vehicles.add(truck);
		
		truck = vehicleBuilder.setBrand("Man")
				.setModel("Big")
				.setAvailable(true)
				.setDailyPrice(new BigDecimal(12000))
				.setLicenseCategory(LicenseCategory.C)
				.setPlateNumber("MAN123")
				.getTruck();
		this.vehicles.add(truck);
		
		MotorBike bike = vehicleBuilder.setBrand("Yamaha")
				.setModel("PSR100")
				.setAvailable(true)
				.setDailyPrice(new BigDecimal(8000))
				.setLicenseCategory(LicenseCategory.A)
				.setPlateNumber("YAMAHA123")
				.getMotorBike();
		this.vehicles.add(bike);
		
		bike = vehicleBuilder.setBrand("Suzuki")
				.setModel("SR500")
				.setAvailable(true)
				.setDailyPrice(new BigDecimal(7600))
				.setLicenseCategory(LicenseCategory.A)
				.setPlateNumber("SUZUKI123")
				.getMotorBike();
		this.vehicles.add(bike);
	}
	
	public List<Vehicle> getAllVehicles(){		
		return this.vehicles;
	}
	
	public void addReservations(Customer customer, Vehicle vehicle) {
		// todo
	}
	
	public List<Reservation> getReservations(){
		return this.reservations;
	}

	public void addNewCustomer(Customer customer) {
		if (customer == null) {
			//throw new Exception();
		}else {
			this.customers.add(customer);
		}
	}
	
}
